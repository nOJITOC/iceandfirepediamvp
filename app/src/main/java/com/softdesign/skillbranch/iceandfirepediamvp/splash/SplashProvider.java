package com.softdesign.skillbranch.iceandfirepediamvp.splash;

import com.softdesign.skillbranch.iceandfirepediamvp.core.database.DatabaseManager;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.Book;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.DaoSession;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.House;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.SwornMember;
import com.softdesign.skillbranch.iceandfirepediamvp.splash.data.HouseId;
import com.softdesign.skillbranch.iceandfirepediamvp.splash.data.RestService;
import com.softdesign.skillbranch.iceandfirepediamvp.splash.data.ServiceGenerator;
import com.softdesign.skillbranch.iceandfirepediamvp.splash.data.res.BookRes;
import com.softdesign.skillbranch.iceandfirepediamvp.splash.data.res.CharacterRes;
import com.softdesign.skillbranch.iceandfirepediamvp.splash.data.res.HouseRes;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class SplashProvider implements ISplashProvider {

    private RestService mRestService;


    private DaoSession mDaoSession;

    private int countOfCharacters;

    public int getCountOfCharacters() {
        return countOfCharacters;
    }

    public void incrementtOfCharacters(Integer countOfCharacters) {
        this.countOfCharacters += countOfCharacters;
    }

    public SplashProvider() {
        mDaoSession = DatabaseManager.getInstance().getDaoSession();
        mRestService = ServiceGenerator.createService(RestService.class);
    }

    @Override
    public Observable<Integer> countOfCharactersInApi() {
        return Observable.combineLatest(
                getHouse(HouseId.LANNISTER),
                getHouse(HouseId.STARK),
                getHouse(HouseId.TARGARYEN),
                (starkHouse, lanisterHouse, targaryenHouse) ->
                        (starkHouse.getSwornMembers().size() +
                                lanisterHouse.getSwornMembers().size() +
                                targaryenHouse.getSwornMembers().size())
        ).observeOn(Schedulers.newThread())
                .subscribeOn(AndroidSchedulers.mainThread());
    }

    //region ====================retrofit===================================
    private Observable<List<BookRes>> getBooks() {
        return mRestService.getBooks();
    }

    private Observable<HouseRes> getHouse(Long houseId) {
        return mRestService.getHouseById(houseId);
    }

    private Observable<CharacterRes> getCharacterByUrl(String url) {
        return mRestService.getCharacterByUrl(url);
    }

    private Observable<CharacterRes> getCharacterById(Long id) {
        return mRestService.getCharacterById(id);
    }

    //endregion


    //region =========================dbsave==============================
    @Override
    public void addBooksToDb() {
        getBooks()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .cache()
                .subscribe(bookReses -> {
                            List<Book> books = new ArrayList<>();
                            for (BookRes bookRes : bookReses) {
                                books.add(new Book(bookRes));
                            }
                            mDaoSession.getBookDao().insertOrReplaceInTx(books);
                        }

                );
    }

    @Override
    public Observable<SwornMember> loadCharacters(Long id) {
        return getHouse(id)
                .map(houseRes -> {
                    incrementtOfCharacters(houseRes.getSwornMembers().size());
                    mDaoSession.getHouseDao().insertOrReplace(new House(houseRes));
                    return houseRes.getSwornMembers();
                }).flatMap(Observable::from)
                .flatMap(url -> mRestService.getCharacterByUrl(url),
                        Runtime.getRuntime().availableProcessors())
                .map(characterRes -> {
                    SwornMember member = new SwornMember(characterRes, id);
                    checkParents(member, id);
                    if (mDaoSession.getSwornMemberDao().getKey(member) == null) {
                        mDaoSession.getSwornMemberDao().insertOrReplace(member);
                    }
                    return member;
                })
                .observeOn(Schedulers.newThread())
                .subscribeOn(AndroidSchedulers.mainThread());
    }


    private void checkParents(SwornMember member, Long houseId) {

        if (member.getFather() != null) {
            getCharacterById(member.getFather())
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(memberRes -> {
                            SwornMember father = new SwornMember(memberRes, houseId);
                            incrementtOfCharacters(1);
                            mDaoSession.insertOrReplace(father);
                            checkParents(father, houseId);
                    });
        }
        if (member.getMother() != null) {
            getCharacterById(member.getMother())
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(memberRes -> {
                            SwornMember mother = new SwornMember(memberRes, houseId);
                            incrementtOfCharacters(1);
                            mDaoSession.insertOrReplace(mother);
                            checkParents(mother, houseId);
                    })
            ;
        }

    }

    //endregion


}