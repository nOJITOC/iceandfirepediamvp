package com.softdesign.skillbranch.iceandfirepediamvp.core.layers.view;


public interface IView {
    void showMessage(String message);
    void showError(Throwable t);
    void showLoad();
    void hideLoad();
}
