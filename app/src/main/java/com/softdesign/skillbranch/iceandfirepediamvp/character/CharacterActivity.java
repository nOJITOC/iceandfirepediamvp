package com.softdesign.skillbranch.iceandfirepediamvp.character;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.softdesign.skillbranch.iceandfirepediamvp.core.utils.BaseActivity;
import com.softdesign.skillbranch.iceandfirepediamvp.houses.HouseFragment;
import com.softdesign.skillbranch.iceandfirepediamvp.R;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.SwornMember;

public class CharacterActivity extends BaseActivity implements ICharacterView {


    CoordinatorLayout mCoordinatorLayout;
    Toolbar mToolbar;
    CollapsingToolbarLayout mCollapsingToolbar;
    ImageView mHerbImage;
    TextView mWordsTv, mBornTv, mDiedTv, mTitlesTv, mAliasesTv;
    Button mFatherBtn, mMotherBtn;
    public CharacterPresenter mPresenter = CharacterPresenter.getInstance();
    Long charId;

    //region ==========================Life cicle=============================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_character);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mCollapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.ctl);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.root_coordinator_layout);
        mHerbImage = (ImageView) findViewById(R.id.imageHouses);
        mWordsTv = (TextView) findViewById(R.id.words_tv);
        mBornTv = (TextView) findViewById(R.id.born_tv);
        mDiedTv = (TextView) findViewById(R.id.died_tv);
        mTitlesTv = (TextView) findViewById(R.id.titles_tv);
        mAliasesTv = (TextView) findViewById(R.id.aliases_tv);
        mFatherBtn = (Button) findViewById(R.id.father_btn);
        mMotherBtn = (Button) findViewById(R.id.mother_btn);
        mPresenter.attachView(this);
        if (savedInstanceState == null)
            charId = getIntent().getLongExtra(HouseFragment.CHARACTER_ID, 1l);
        else
            charId = savedInstanceState.getLong(HouseFragment.CHARACTER_ID);
        mPresenter.showCharacter(charId);
        setupToolbar();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return mPresenter.showChild();
            default:
                return false;
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putLong(HouseFragment.CHARACTER_ID, charId);
        super.onSaveInstanceState(outState);

    }
    //endregion

    //region ======================ICharacterView=================================
    @Override
    public void showCharacterInfo(SwornMember character, String words, int herb) {
        mCollapsingToolbar.setTitle(character.getName());
        setInfoAboutCharacter(mWordsTv, words);
        setInfoAboutCharacter(mBornTv, character.getBorn());
        setInfoAboutCharacter(mDiedTv, character.getDied());
        setInfoAboutCharacter(mTitlesTv, character.getTitles());
        setInfoAboutCharacter(mAliasesTv, character.getAliases());
        mHerbImage.setImageResource(herb);

        if (mPresenter.getBook(character.getId()) != null)
            showMessage(mPresenter.getBook(character.getId()));
        setParent(mFatherBtn, character.getFather());
        setParent(mMotherBtn, character.getMother());
    }

    public void setInfoAboutCharacter(TextView tv, String title) {
        if (title.isEmpty()) {
            ((View) tv.getParent()).setVisibility(View.GONE);
        } else {
            ((View) tv.getParent()).setVisibility(View.VISIBLE);
            tv.setText(title);
        }
    }

    public void setParent(Button parent, final Long parentId) {

        if (parentId != null) {
            SwornMember parent1 = mPresenter.getParent(parentId);
            if (parent1 != null) {
                ((View) parent.getParent()).setVisibility(View.VISIBLE);
                parent.setText(parent1.getName());
                parent.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                charId = parentId;
                                mPresenter.showParent(parentId);
                            }
                        });
            }
        } else {
            ((View) parent.getParent()).setVisibility(View.GONE);
        }
    }

    public void showFather(Long id) {
        mPresenter.showParent(id);
    }


    //endregion


}
