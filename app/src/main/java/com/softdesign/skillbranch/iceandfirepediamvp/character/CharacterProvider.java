package com.softdesign.skillbranch.iceandfirepediamvp.character;

import com.softdesign.skillbranch.iceandfirepediamvp.core.database.DatabaseManager;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.DaoSession;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.HouseDao;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.SwornMember;
import com.softdesign.skillbranch.iceandfirepediamvp.houses.data.HouseDrawables;


public class CharacterProvider  implements ICharacterProvider{
    private DaoSession mDaoSession;
    private HouseDao mHouseDao;

    public CharacterProvider() {
        mDaoSession = DatabaseManager.getInstance().getDaoSession();
        mHouseDao = mDaoSession.getHouseDao();
    }

    @Override
    public Integer getHerb(Long characterId) {
        return HouseDrawables.getHerb(mDaoSession.getSwornMemberDao().load(characterId).getHouseRemoteId());
    }

    @Override
    public String getWords(Long characterId) {
        return mHouseDao.load(mDaoSession.getSwornMemberDao().load(characterId).getHouseRemoteId()).getWords();
    }

    @Override
    public SwornMember getCharacter(Long characterId) {
        return mDaoSession.getSwornMemberDao().load(characterId);
    }
}
