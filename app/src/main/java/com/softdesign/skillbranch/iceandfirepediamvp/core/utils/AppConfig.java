package com.softdesign.skillbranch.iceandfirepediamvp.core.utils;


public interface AppConfig {

    String BASE_URL = "http://anapioficeandfire.com/api/";
    long MAX_CONNECT_TIMEOUT = 1000 * 30;
    long MAX_READ_TIMEOUT = 1000 * 30;
    long START_DELAY = 1500;
}
