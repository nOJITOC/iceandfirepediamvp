package com.softdesign.skillbranch.iceandfirepediamvp.houses;

import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.House;
import com.softdesign.skillbranch.iceandfirepediamvp.core.layers.view.IView;

import java.util.List;

public interface IHouseView extends IView{
    void setPages(List<House> houses, List<Integer> IconIds, List<String> titles);
    void setubDrawer(List<String> houseNames);

}
