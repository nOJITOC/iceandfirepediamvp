package com.softdesign.skillbranch.iceandfirepediamvp.houses;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.softdesign.skillbranch.iceandfirepediamvp.R;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.SwornMember;
import com.softdesign.skillbranch.iceandfirepediamvp.character.CharacterActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link ListFragment} subclass.
 */
public class HouseFragment extends Fragment {

    public static String CHARACTER_ID = "CHARACTER_ID";
    RecyclerView mRecyclerView;
    private List<SwornMember> mMemberList = new ArrayList<>();
    SwornMembersAdapter.CustomItemClickListener mListener = position -> {
        Intent toCharacterActivity = new Intent(getActivity(), CharacterActivity.class);
        toCharacterActivity.putExtra(CHARACTER_ID, mMemberList.get(position).getId());
        startActivity(toCharacterActivity);
    };
    private int mDrawableId;
    SwornMembersAdapter mAdapter;
    private String mWords;

    public HouseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sworn_members, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.members_list);
        RecyclerView.LayoutManager layoutManager;
        layoutManager = new LinearLayoutManager(getContext());
        mAdapter = new SwornMembersAdapter(mMemberList, mWords, mDrawableId, mListener);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        return rootView;
    }


    public void setHouseMembers(List<SwornMember> members) {
        if (mAdapter != null)
            mAdapter.setItems(members);
        mMemberList = members;
    }


    public void setDrawable(int drawableId) {
        mDrawableId = drawableId;
    }

    public void setWords(String words) {
        mWords = words;
    }
}
