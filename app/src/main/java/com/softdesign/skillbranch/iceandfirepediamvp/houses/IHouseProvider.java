package com.softdesign.skillbranch.iceandfirepediamvp.houses;

import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.House;

import java.util.List;


public interface IHouseProvider {
    List<House> getHouses();
    Integer getIcon(Long houseId);
}
