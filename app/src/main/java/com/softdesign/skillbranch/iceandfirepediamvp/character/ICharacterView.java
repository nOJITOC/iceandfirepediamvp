package com.softdesign.skillbranch.iceandfirepediamvp.character;

import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.SwornMember;
import com.softdesign.skillbranch.iceandfirepediamvp.core.layers.view.IView;


public interface ICharacterView extends IView {
    void showCharacterInfo(SwornMember character, String words, int herb);

}
