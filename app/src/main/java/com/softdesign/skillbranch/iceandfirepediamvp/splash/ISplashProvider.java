package com.softdesign.skillbranch.iceandfirepediamvp.splash;

import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.SwornMember;

import rx.Observable;


public interface ISplashProvider {
    Observable<Integer> countOfCharactersInApi();
    void addBooksToDb();
    Observable<SwornMember> loadCharacters(Long id);
    int getCountOfCharacters();

}
