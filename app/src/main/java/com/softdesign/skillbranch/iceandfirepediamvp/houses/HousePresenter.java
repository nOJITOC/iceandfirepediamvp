package com.softdesign.skillbranch.iceandfirepediamvp.houses;

import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.House;
import com.softdesign.skillbranch.iceandfirepediamvp.core.layers.presenter.Presenter;

import java.util.ArrayList;
import java.util.List;


public class HousePresenter extends Presenter<IHouseView> {
    IHouseProvider mProvider;

    public HousePresenter() {
        mProvider = new HouseProvider();
    }

    public void loadData() {
        List<String> titles = new ArrayList<>();
        List<Integer> icons = new ArrayList<>();
        for (House house : mProvider.getHouses()) {
            icons.add(mProvider.getIcon(house.getId()));
            titles.add(house.getName().split(" ")[1]);
        }
        getView().setPages(mProvider.getHouses(),icons,titles);
        getView().setubDrawer(titles);
    }
}