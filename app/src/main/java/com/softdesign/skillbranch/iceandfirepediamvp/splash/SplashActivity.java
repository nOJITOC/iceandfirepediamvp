package com.softdesign.skillbranch.iceandfirepediamvp.splash;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;

import com.softdesign.skillbranch.iceandfirepediamvp.R;
import com.softdesign.skillbranch.iceandfirepediamvp.core.utils.BaseActivity;
import com.softdesign.skillbranch.iceandfirepediamvp.core.utils.NetworkStatusChecker;
import com.softdesign.skillbranch.iceandfirepediamvp.houses.HouseActivity;


public class SplashActivity extends BaseActivity implements ISplashView {

    private static final String TAG = "Splash Activity";
    SplashPresenter mPresenter;
    Handler mHandler;

    //region ================Life cicle=======================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        prepareProgressDialog();

        mPresenter = new SplashPresenter();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.attachView(this);
        if(NetworkStatusChecker.isNetworkAvailable(this))
        mPresenter.prepareView();
        else {
            showMessage(getString(R.string.sorry_network));
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toHousesActivity();
                }
            }, Snackbar.LENGTH_LONG);

        }
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView(this);
        super.onDestroy();
    }

    //endregion

    //region ===================ISplashView====================================
    @Override
    public void toHousesActivity() {
                Intent toMainActivity = new Intent(SplashActivity.this, HouseActivity.class);
                startActivity(toMainActivity);
    }







    @Override
    public void updateLoad(Integer currentMembersCount, Integer membersCount) {
        int progress = (int) ((double) currentMembersCount / membersCount * 100);
        Message msg = mHandler.obtainMessage();
        msg.arg1 = progress;
        mHandler.sendMessage(msg);
    }

    @Override
    public void showLoad() {
        mProgressDialog.show();
    }



    //endregion
    private void prepareProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setMessage(getString(R.string.progress_dialog_text));
        mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                int totalProgress = msg.arg1;
                mProgressDialog.setProgress(totalProgress);
                if(totalProgress>=100)
                    mProgressDialog.hide();
                super.handleMessage(msg);
            }
        };
    }
}


