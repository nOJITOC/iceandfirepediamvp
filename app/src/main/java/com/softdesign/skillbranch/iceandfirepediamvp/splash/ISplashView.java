package com.softdesign.skillbranch.iceandfirepediamvp.splash;



import com.softdesign.skillbranch.iceandfirepediamvp.core.layers.view.IView;

public interface ISplashView extends IView {
    void toHousesActivity();
    void updateLoad(Integer currentMembersCount, Integer membersCount);

}
