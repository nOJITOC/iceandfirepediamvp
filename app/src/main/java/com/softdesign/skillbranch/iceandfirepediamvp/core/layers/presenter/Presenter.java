package com.softdesign.skillbranch.iceandfirepediamvp.core.layers.presenter;


import android.support.annotation.NonNull;

import com.softdesign.skillbranch.iceandfirepediamvp.core.layers.view.IView;

public class Presenter<V extends IView> {

    private V mView;


    public void attachView(@NonNull V view) {
        mView = view;
        onViewAttached(view);
    }

    public void detachView(@NonNull V view) {
        mView = null;
        onViewDetached(view);

    }


    public V getView() {
        return mView;
    }





    protected void onViewAttached(V view) {
    }

    protected void onViewDetached(V view) {
    }


}