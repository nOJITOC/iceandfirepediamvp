package com.softdesign.skillbranch.iceandfirepediamvp.houses;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.softdesign.skillbranch.iceandfirepediamvp.R;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.House;
import com.softdesign.skillbranch.iceandfirepediamvp.core.utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class HouseActivity extends BaseActivity implements IHouseView {

    private static final String TAB_POSITION = "TAB_POSITION";
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private Toolbar mToolbar;
    private ListView mListView;
    private HousePresenter mPresenter;
    DrawerLayout mDrawer;
    ViewPagerAdapter mPagerAdapter;
    int tabPosition;
    //region =======================Life cicle================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_house);

        mViewPager = (ViewPager) findViewById(R.id.houses_vp);
        mTabLayout = (TabLayout) findViewById(R.id.house_tl);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mListView = (ListView) findViewById(R.id.left_drawer);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        if (savedInstanceState != null) {
            tabPosition = savedInstanceState.getInt(TAB_POSITION);
        }
        mPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter = new HousePresenter();
        mPresenter.attachView(this);
        mPresenter.loadData();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    //endregion


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(TAB_POSITION, mViewPager.getCurrentItem());
        super.onSaveInstanceState(outState);
    }

    public void setPages(List<HouseFragment> fragments, List<String> titles) {
        mPagerAdapter.setPages(fragments, titles);

    }
    @Override
    public void setPages(List<House> houses, List<Integer> IconIds, List<String> titles) {
        List<HouseFragment> pages = new ArrayList<>();
        for (int i = 0; i < houses.size(); i++) {
            pages.add(prepareHousePage(houses.get(i), IconIds.get(i)));
        }
        setPages(pages, titles);
    }

    public HouseFragment prepareHousePage(House house, Integer icon) {
        HouseFragment current = new HouseFragment();
        current.setHouseMembers(house.getSwornMembers());
        current.setDrawable(icon);
        current.setWords(house.getWords());
        return current;
    }

    @Override
    public void setubDrawer(List<String> houseNames) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.driwer_list_item, houseNames);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener((adapterView, view, i, l) -> {
            mTabLayout.getTabAt(i).select();
            mDrawer.closeDrawer(GravityCompat.START);
        });
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.open_menu, R.string.close_menu);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView(this);
        super.onDestroy();
    }
}
