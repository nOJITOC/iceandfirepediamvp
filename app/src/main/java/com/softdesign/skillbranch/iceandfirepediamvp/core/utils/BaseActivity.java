package com.softdesign.skillbranch.iceandfirepediamvp.core.utils;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import com.softdesign.skillbranch.iceandfirepediamvp.BuildConfig;
import com.softdesign.skillbranch.iceandfirepediamvp.core.layers.view.IView;
import com.softdesign.skillbranch.iceandfirepediamvp.R;


public class BaseActivity extends AppCompatActivity implements IView {
    private static final long PD_DELAY = 2000;
    protected ProgressDialog mProgressDialog;

    @Override
    public void showMessage(String message) {
        Snackbar.make(((ViewGroup) findViewById(android.R.id.content)).getChildAt(0), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable t) {
        if (BuildConfig.DEBUG) {
            showMessage(t.getMessage());
            t.printStackTrace();
        } else {
            showMessage(getString(R.string.sorry_error));
            // TODO: 20.10.2016 send error stacktrace to crashliticks
        }
    }

    @Override
    public void showLoad() {
        this.mProgressDialog = ProgressDialog.show(this, null, getString(R.string.progress_dialog_text), true, false);

    }

    @Override
    public void hideLoad() {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (mProgressDialog != null)
                        mProgressDialog.hide();
                }
            },PD_DELAY);

    }


}
