package com.softdesign.skillbranch.iceandfirepediamvp.core.utils;

import android.content.SharedPreferences;



public class PreferencesManager {
    private static PreferencesManager INSTANCE = new PreferencesManager();
    private SharedPreferences mSharedPreferences;
    public PreferencesManager(){
        mSharedPreferences = IceAndFireApplication.getSharedPreferences();
    }


    public static PreferencesManager getInstance() {
        return INSTANCE;
    }

    public SharedPreferences getSharedPreferences() {
        return mSharedPreferences;
    }
}
