package com.softdesign.skillbranch.iceandfirepediamvp.character;

import android.view.View;

import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.SwornMember;
import com.softdesign.skillbranch.iceandfirepediamvp.core.layers.presenter.Presenter;

import java.util.LinkedList;
import java.util.Queue;

public class CharacterPresenter extends Presenter<ICharacterView> {
    private static CharacterPresenter sInstance = new CharacterPresenter();
    ICharacterProvider mProvider;
    Queue<Long> characterIds;
    Long currentCharacter;

    public CharacterPresenter() {
        mProvider = new CharacterProvider();
        characterIds = new LinkedList<>();
    }

    public static CharacterPresenter getInstance() {
        return sInstance;
    }

    public void showCharacter(Long charId) {
        currentCharacter = charId;
        getView().showCharacterInfo(mProvider.getCharacter(charId), mProvider.getWords(charId), mProvider.getHerb(charId));
    }

    public boolean showChild() {
        Long currentId = characterIds.poll();
        if (currentId == null)
            return false;
        else {
            showCharacter(currentId);
            return true;
        }
    }

    public String getBook(Long id) {
        if (mProvider.getCharacter(id).getDied().isEmpty())
            return null;
        return mProvider.getCharacter(id).getLastBook().getName();
    }

    public String getName(Long parentId) {
        return mProvider.getCharacter(parentId).getName();
    }

    public void showParent(Long parentId) {
        characterIds.add(currentCharacter);
        showCharacter(parentId);
    }



    public SwornMember getParent(Long id) {
        return mProvider.getCharacter(id);
    }

}
