package com.softdesign.skillbranch.iceandfirepediamvp.houses;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;


public class ViewPagerAdapter extends FragmentPagerAdapter {
    private static List<HouseFragment>  mHouses = new ArrayList<>();
    private static List<String> mTitle = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        mHouses = new ArrayList<>();
        mTitle = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return mHouses.get(position);
    }


    @Override
    public int getCount() {
        return mHouses.size();
    }

    public void setPages(List<HouseFragment> fragments, List<String> titles){
        mHouses = fragments;
        mTitle = titles;
        notifyDataSetChanged();

    }



    @Override
    public CharSequence getPageTitle(int position) {
        return mTitle.get(position);
    }
}
