package com.softdesign.skillbranch.iceandfirepediamvp.splash;

import android.os.Handler;
import android.os.Message;

import com.softdesign.skillbranch.iceandfirepediamvp.core.database.DatabaseManager;
import com.softdesign.skillbranch.iceandfirepediamvp.core.layers.presenter.Presenter;
import com.softdesign.skillbranch.iceandfirepediamvp.core.utils.NetworkStatusChecker;
import com.softdesign.skillbranch.iceandfirepediamvp.core.utils.PreferencesManager;
import com.softdesign.skillbranch.iceandfirepediamvp.splash.data.HouseId;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class SplashPresenter extends Presenter<ISplashView>{
    private ISplashProvider mModel;
    PreferencesManager mPreferencesManager;
    int mMembersCount;
    int currentMembersCount;
    Handler mHandler;

    public SplashPresenter() {
        mModel = new SplashProvider();
        mPreferencesManager = PreferencesManager.getInstance();
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                getView().hideLoad();
                super.handleMessage(msg);
            }
        };
    }

    public void prepareView() {
        getView().showLoad();
        mModel.countOfCharactersInApi()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(count -> {
                    mMembersCount = count;
                    if (DatabaseManager.getInstance().getDaoSession().getSwornMemberDao().count() >= count) {
                        getView().toHousesActivity();
                    }
                    else
                        saveData();
                });
    }

    public void saveData() {
        mModel.addBooksToDb();
        Observable.concat(
                mModel.loadCharacters(HouseId.LANNISTER),
                mModel.loadCharacters(HouseId.STARK),
                mModel.loadCharacters(HouseId.TARGARYEN)
        )
                .observeOn(Schedulers.newThread())
                .subscribeOn(AndroidSchedulers.mainThread()).subscribe(swornMember -> {
            currentMembersCount++;
            getView().updateLoad(currentMembersCount, mMembersCount);
            if (currentMembersCount >= mMembersCount) {
                getView().toHousesActivity();
            }
        });
    }

}
