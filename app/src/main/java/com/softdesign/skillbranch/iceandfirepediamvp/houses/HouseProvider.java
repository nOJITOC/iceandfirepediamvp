package com.softdesign.skillbranch.iceandfirepediamvp.houses;

import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.DaoSession;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.House;
import com.softdesign.skillbranch.iceandfirepediamvp.core.utils.IceAndFireApplication;
import com.softdesign.skillbranch.iceandfirepediamvp.houses.data.HouseDrawables;

import java.util.List;


public class HouseProvider implements IHouseProvider {
    private DaoSession mDaoSession;

    public HouseProvider() {
        mDaoSession = IceAndFireApplication.getDaoSession();
    }

    @Override
    public List<House> getHouses() {
        return mDaoSession.getHouseDao().loadAll();
    }

    @Override
    public Integer getIcon(Long houseId) {
        return HouseDrawables.getIcon(houseId);
    }
}
