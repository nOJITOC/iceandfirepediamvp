package com.softdesign.skillbranch.iceandfirepediamvp.splash.data;







import com.softdesign.skillbranch.iceandfirepediamvp.splash.data.res.BookRes;
import com.softdesign.skillbranch.iceandfirepediamvp.splash.data.res.CharacterRes;
import com.softdesign.skillbranch.iceandfirepediamvp.splash.data.res.HouseRes;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;
import rx.Observable;

public interface RestService {

    @GET("houses?pageSize=50&page={page}")
    Call<HouseRes> getHouses(@Path("page") String page);


    @GET("houses/{id}")
    Observable<HouseRes> getHouseById(@Path(("id")) Long id);
    @GET("characters/{id}")
    Observable<CharacterRes> getCharacterById(@Path("id") Long id);
    @GET()
    Observable<CharacterRes> getCharacterByUrl(@Url String url);
    @GET("books?pageSize=50")
    Observable<List<BookRes>> getBooks();
}
