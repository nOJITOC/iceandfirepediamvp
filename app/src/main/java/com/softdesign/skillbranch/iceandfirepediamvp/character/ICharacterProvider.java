package com.softdesign.skillbranch.iceandfirepediamvp.character;

import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.SwornMember;


public interface ICharacterProvider {
    SwornMember getCharacter(Long characterId);
    Integer getHerb(Long characterId);
    String getWords(Long characterId);

}
