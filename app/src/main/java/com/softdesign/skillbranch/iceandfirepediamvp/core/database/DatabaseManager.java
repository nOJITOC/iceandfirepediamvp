package com.softdesign.skillbranch.iceandfirepediamvp.core.database;

import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.DaoSession;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.SwornMember;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.SwornMemberDao;
import com.softdesign.skillbranch.iceandfirepediamvp.core.utils.IceAndFireApplication;


public class DatabaseManager {

    private static DatabaseManager sInstance = new DatabaseManager();
    private DaoSession mDaoSession;

    public DatabaseManager() {
        mDaoSession = IceAndFireApplication.getDaoSession();
    }

    public static DatabaseManager getInstance() {
        if(sInstance == null)
            sInstance = new DatabaseManager();
        return sInstance;
    }

    public SwornMember loadCharacterByApiId(Long father) {
        SwornMember member = null;
        try {
            member = mDaoSession.queryBuilder(SwornMember.class)
                    .where(SwornMemberDao.Properties.ApiId.eq(father))
                    .build()
                    .unique();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return member;
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }
}
