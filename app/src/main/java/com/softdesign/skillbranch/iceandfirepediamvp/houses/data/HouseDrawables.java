package com.softdesign.skillbranch.iceandfirepediamvp.houses.data;

import com.softdesign.skillbranch.iceandfirepediamvp.splash.data.HouseId;
import com.softdesign.skillbranch.iceandfirepediamvp.R;


public class HouseDrawables {
    public static int getIcon(Long id) {

        switch (id.intValue()) {
            default:
            case (int)HouseId.STARK: {
                return R.drawable.stark_icon;
            }
            case (int)HouseId.LANNISTER: {
                return R.drawable.lannister_icon;
            }
            case (int)HouseId.TARGARYEN: {
                return R.drawable.targaryen_icon;
            }
        }
    }

    public static int getHerb(Long id) {
        switch (id.intValue()) {
            default:
            case (int)HouseId.LANNISTER: {
                return R.drawable.lannister;
            }
            case (int)HouseId.STARK: {
                return R.drawable.stark;
            }
            case (int)HouseId.TARGARYEN: {
                return R.drawable.targaryen;
            }
        }
    }
}