package com.softdesign.skillbranch.iceandfirepediamvp.splash.data;

import android.support.annotation.IntDef;

@IntDef({
        HouseId.LANNISTER,
        HouseId.STARK,
        HouseId.TARGARYEN})
public @interface HouseId {

    long LANNISTER = 229l;
    long STARK = 362l;
    long TARGARYEN = 378l;
}