package com.softdesign.skillbranch.iceandfirepediamvp.houses;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.softdesign.skillbranch.iceandfirepediamvp.R;
import com.softdesign.skillbranch.iceandfirepediamvp.core.database.models.SwornMember;


import java.util.List;


public class SwornMembersAdapter extends RecyclerView.Adapter<SwornMembersAdapter.SwornMemberViewHolder> {
    private final int mDrawableId;
    CustomItemClickListener mListener;
    List<SwornMember> mMemberList;
    Context mContext;
    String mWords;

    public SwornMembersAdapter(List<SwornMember> members, String words, int drawableId, CustomItemClickListener listener) {
        mMemberList = members;
        mWords = words;
        mListener = listener;
        mDrawableId = drawableId;
    }

    @Override
    public SwornMemberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.item_list_member, parent, false);
        return new SwornMemberViewHolder(convertView, mListener);
    }

    public void setItems(List<SwornMember> memberList) {
        if (memberList != null) {
            mMemberList = memberList;
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBindViewHolder(SwornMemberViewHolder holder, int position) {
        String titles = mMemberList.get(position).getTitles();
        Glide.with(mContext)
                .load("")
                .placeholder(mDrawableId)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .fitCenter()
                .centerCrop()
                .into(holder.mImageView);
        holder.mNameTv.setText(mMemberList.get(position).getName());
        holder.mWordsTv.setText(titles.isEmpty() ? mWords : titles);


    }

    @Override
    public int getItemCount() {
        return mMemberList.size();
    }

    public class SwornMemberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CustomItemClickListener mItemClickListener;
        ImageView mImageView;
        TextView mNameTv, mWordsTv;

        public SwornMemberViewHolder(View itemView, CustomItemClickListener listener) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.emblem);
            mNameTv = (TextView) itemView.findViewById(R.id.name_tv);
            mWordsTv = (TextView) itemView.findViewById(R.id.words_tv);
            mItemClickListener = listener;
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            Log.e("Exception", " onClick: " + view.getId());
            if (mItemClickListener != null)
                mItemClickListener.onItemClick(getAdapterPosition());
        }
    }


    public interface CustomItemClickListener {
        void onItemClick(int position);
    }
}
